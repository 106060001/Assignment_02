# Assignment_02

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|Sound effects|5%|Y|
|UI|5%|Y|
|Leaderboard|5%|N|
|Appearance|5%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Multi player game|20%|N|
Enhanced items
|Bullet automatic aiming|5%|Y|
|Unique bullet|5%|Y|
|Little helper|5%|N|
|Boss|2.5%|N|

# Components Description : 
遊戲是使用鍵盤進行遊玩，上下左右鍵操控戰鬥機移動，空白鍵射擊，Ｚ鍵是大絕招，需要時間冷卻，按下後敵機將全數死亡，子彈也會被清除，左下角顯示可使用次數，最多只能累積三次；Ｘ鍵是小招，將隱身一段時間，隱身期間主角將不會受到傷害，右下角顯示可使用次數。另外，使用絕招擊落敵人將不會掉落道具。音樂部分，進入遊戲後會開始撥放背景音樂，按下Ｍ鍵可以靜音，若再次按下會解除靜音，Q和W鍵分別為音量增加鍵和音量減少鍵。另外，靜音模式下按增加音量無法跳出靜音模式。暫停部分，按下Ｐ鍵可以暫停，再次按下解除暫停。遊戲機制部分，遊戲開始時會有１０台敵方飛機，每擊落５台飛機敵方飛機總數量將會增加，若使用大招擊落則固定增加兩台敵機。每次用子彈擊落敵方飛機有機會掉落道具，有黑底紅點、黑底白點、黑底橘點和黑底綠點，分別是增加大招使用次數，增加小招使用次數，子彈切換成穿甲模式，子彈切換成自瞄模式。普通子彈碰到敵方飛機後將會消失，穿甲模式的子彈可以穿透敵機攻擊到後方的敵機。另外，子彈模式只能存在一種，不會同時可以穿透又自瞄。遊戲畫面左上方顯示得分，擊落一架敵機得２０分，右上方顯示剩餘生命，被敵人子彈命中或被敵機撞到減少，敵人子彈會自動瞄準玩家。