var loadState = {
    preload: function () {
        var loadingLabel = game.add.text(game.width/2, 150,'loading...', { font: '30px Arial', fill: '#ffffff'});
        loadingLabel.anchor.setTo(0.5,0.5);
        var progressBar = game.add.sprite(game.width/2, 200, 'progressBar');
        progressBar.anchor.setTo(0.5,0.5);
        game.load.setPreloadSprite(progressBar);
        game.load.spritesheet('player', 'player.png', 34, 43);
        game.load.image('bg', 'bg.jpg');
        game.load.image('enemy1', 'enemy1.png');
        game.load.image('bullet', 'bullet.png');
        game.load.image('enemybullet', 'enemybullet3.png');
        game.load.image('heart', 'heart.png');
        game.load.image('pixel', 'pixel.png');
        game.load.image('mute', 'mute.png');
        game.load.image('sound', 'sound.png');
        game.load.image('pause', 'pause.png');
        game.load.image('continue', 'continue.png');
        game.load.image('skill1', 'skill1.png');
        game.load.image('skill2', 'skill2.png');
        game.load.image('skill3', 'skill3.png');
        game.load.image('skill4', 'skill4.png');
        game.load.audio('bgm', ['bgm.ogg', 'bgm.mp3']);
        game.load.audio('playerdie', ['playerdie.ogg', 'playerdie.mp3']);
        game.load.audio('shoot', ['shoot.ogg', 'shoot.mp3']);
        game.load.audio('skill1', ['skill1.ogg', 'skill1.mp3']);
        game.load.audio('skill2', ['skill2.ogg', 'skill2.mp3']);
    },
    create: function() {
        game.state.start('menu');
    }
};