var menuState = {
    create: function() {
        game.add.image(0, 0, 'bg');
        var nameLabel = game.add.text(game.width/2, -50, 'Raiden', {font: '200px Arial', fill: '#ffffff'});
        nameLabel.anchor.setTo(0.5, 0.5);
        var startLabel = game.add.text(game.width/2, game.height-80, 'press the spaceBar to start', {font: '25px Arial', fill: '#000000'});
        startLabel.anchor.setTo(0.5, 0.5);
        var spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        spaceKey.onDown.add(this.start, this);
        var tween = game.add.tween(nameLabel);
        tween.to({y:150}, 1000);
        tween.start();
        startLabel.alpha = 0;
        var tween2 = game.add.tween(startLabel).to( { alpha: 1 }, 500, "Linear", true, 0, -1);
        tween2.yoyo(true, 1000);
        if (!localStorage.getItem('bestScore')) {
            localStorage.setItem('bestScore', 0);
        }
        if (game.global.score > localStorage.getItem('bestScore')) {
            localStorage.setItem('bestScore', game.global.score);
        }
        var scoretext = 'score: ' + game.global.score + '\nbest score: ' +
        localStorage.getItem('bestScore');
        var scoreLabel = game.add.text(game.width/2, game.height/2,
        scoretext, { font: '25px Arial', fill: '#ffffff', align: 'center' });
        scoreLabel.anchor.setTo(0.5, 0.5);
    },
    start: function() {
        game.state.start('play');
    }

};