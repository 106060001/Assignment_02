var playState = {
    preload: function(){},
    create: function() {
        game.global.score = 0;
        BG = game.add.tileSprite(0, 0, 900, 800, 'bg');
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        this.player = game.add.sprite(game.width/2, 700, 'player');
        this.player.scale.setTo(0.5, 0.5);
        this.player.anchor.setTo(0.5, 0.5);
        this.cursor = game.input.keyboard.createCursorKeys();
        game.physics.arcade.enable(this.player);
        this.scoreLabel = game.add.text (30, 30, 'score: 0',{ font: '18px Arial', fill: '#ffffff'});
        this.liveLabel = game.add.text(game.world.width-100, 30, 'Lives : ', { font: '18px Arial', fill: '#fff' });
        this.skill1Label = game.add.text (30, game.world.height-100, 'Skill1 : 1',{ font: '18px Arial', fill: '#ffffff'});
        this.skill2Label = game.add.text (game.world.width-100, game.world.height-100, 'Skill2 : 2',{ font: '18px Arial', fill: '#ffffff'}); 
        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        for(var i=0;i<10;i++){
            this.createEnemy();
        }
        this.player.animations.add('rightwalk', [6, 7, 8, 9, 10], 5, false);
        this.player.animations.add('leftwalk', [4, 3, 2, 1, 0], 5, false);  
        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets.createMultiple(30, 'bullet');
        this.bullets.setAll('anchor.x', 0.5);
        this.bullets.setAll('anchor.y', 1);
        this.bullets.setAll('outOfBoundsKill', true);
        this.bullets.setAll('checkWorldBounds', true);
        this.enemyBullets = game.add.group();
        this.enemyBullets.enableBody = true;
        this.enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemyBullets.createMultiple(2000, 'enemybullet');
        this.enemyBullets.setAll('anchor.x', 0.5);
        this.enemyBullets.setAll('anchor.y', 1);
        this.enemyBullets.setAll('outOfBoundsKill', true);
        this.enemyBullets.setAll('checkWorldBounds', true);
        this.fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.skillbutton1 = game.input.keyboard.addKey(Phaser.Keyboard.Z);
        this.skillbutton2 = game.input.keyboard.addKey(Phaser.Keyboard.X);
        this.pausebutton = game.input.keyboard.addKey(Phaser.Keyboard.P);
        this.mutebutton = game.input.keyboard.addKey(Phaser.Keyboard.M);
        this.iVolumebutton = game.input.keyboard.addKey(Phaser.Keyboard.Q);
        this.dVolumebutton = game.input.keyboard.addKey(Phaser.Keyboard.W);
        this.bulletTime = 0;
        this.enemymoveTime = 0;
        this.firingTimer = 0;
        this.lives = game.add.group();
        this.hurted = true;
        for (var i = 0; i < 3; i++) 
        {
            var ship = this.lives.create(game.world.width - 100 + (30 * i), 60, 'heart');
            ship.scale.setTo(0.1, 0.1);
        }
        this.emitter= game.add.emitter (0, 0, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 0;
        this.bgmSound= game.add.audio('bgm');
        this.bgmSound.volume = 0.5;
        this.bgmSound.play();
        this.bgmSound.loop = true;
        this.playerdieSound = game.add.audio('playerdie');
        this.shootSound = game.add.audio('shoot');
        this.skill1Sound = game.add.audio('skill1');
        this.skill2Sound = game.add.audio('skill2');
        this.playerdieSound.volume = 0.5;
        this.shootSound.volume = 0.5;
        this.skill1Sound.volume = 0.5;
        this.skill2Sound.volume = 0.5;
        this.vol = 0.5;
        this.isMute = false;
        this.dVolumebutton.onDown.add(this.dVolume, self);
        this.iVolumebutton.onDown.add(this.iVolume, self);
        this.muteicon = game.add.sprite(game.width/2, game.height/2, 'mute');
        this.soundicon = game.add.sprite(game.width/2, game.height/2, 'sound');
        this.muteicon.alpha = 0;
        this.soundicon.alpha = 0;
        this.muteicon.anchor.setTo(0.5, 0.5);
        this.soundicon.anchor.setTo(0.5, 0.5);
        this.pauseicon = game.add.sprite(game.width/2, game.height/2, 'pause');
        this.continueicon = game.add.sprite(game.width/2, game.height/2, 'continue');
        this.continueicon.scale.setTo(0.5, 0.5);
        this.pauseicon.alpha = 0;
        this.continueicon.alpha = 0;
        this.pauseicon.anchor.setTo(0.5, 0.5);
        this.continueicon.anchor.setTo(0.5, 0.5);
        this.muteTime = 0;
        this.skill3 = false;
        this.skill3Time = 0;
        this.pausebutton.onDown.add(this.unpause, self);
        this.skill1items = game.add.group();
        this.skill1items.enableBody = true;
        this.skill1items.physicsBodyType = Phaser.Physics.ARCADE;
        this.skill1items.createMultiple(5, 'skill1');
        this.skill1items.setAll('anchor.x', 0.5);
        this.skill1items.setAll('anchor.y', 1);
        this.skill1items.setAll('outOfBoundsKill', true);
        this.skill1items.setAll('checkWorldBounds', true);
        this.skill2items = game.add.group();
        this.skill2items.enableBody = true;
        this.skill2items.physicsBodyType = Phaser.Physics.ARCADE;
        this.skill2items.createMultiple(5, 'skill2');
        this.skill2items.setAll('anchor.x', 0.5);
        this.skill2items.setAll('anchor.y', 1);
        this.skill2items.setAll('outOfBoundsKill', true);
        this.skill2items.setAll('checkWorldBounds', true);
        this.skill1Num = 1;
        this.skill2Num = 2;
        this.skill1Cooldown = 0;
        this.skill4 = false;
        this.skill3items = game.add.group();
        this.skill3items.enableBody = true;
        this.skill3items.physicsBodyType = Phaser.Physics.ARCADE;
        this.skill3items.createMultiple(5, 'skill3');
        this.skill3items.setAll('anchor.x', 0.5);
        this.skill3items.setAll('anchor.y', 1);
        this.skill3items.setAll('outOfBoundsKill', true);
        this.skill3items.setAll('checkWorldBounds', true);
        this.skill4items = game.add.group();
        this.skill4items.enableBody = true;
        this.skill4items.physicsBodyType = Phaser.Physics.ARCADE;
        this.skill4items.createMultiple(5, 'skill4');
        this.skill4items.setAll('anchor.x', 0.5);
        this.skill4items.setAll('anchor.y', 1);
        this.skill4items.setAll('outOfBoundsKill', true);
        this.skill4items.setAll('checkWorldBounds', true);
        this.pausecd = false;
    },
    update: function() {

        this.scoreLabel.bringToTop();
        this.liveLabel.bringToTop();
        this.skill1Label.bringToTop();
        this.skill2Label.bringToTop();
        BG.tilePosition.y += 4;
        this.movePlayer();
        if (game.time.now > this.enemymoveTime)
        {   
            this.enemies.forEachAlive(function(enemy){
                if(enemy.x<0){
                    enemy.body.velocity.x = 200;
                }
                else if(enemy.x>900){
                    enemy.body.velocity.x = -200;
                }
                else{
                    enemy.body.velocity.x = game.rnd.integerInRange(-2,2)*100;
                }
                if(enemy.y<0){
                    enemy.body.velocity.y = 200;
                }
                else if(enemy.y>800){
                    enemy.body.velocity.y = -200;
                }
                else{
                    enemy.body.velocity.y = game.rnd.integerInRange(-2,2)*100;
                }
            });
            this.enemymoveTime = game.time.now + 500;
        } 
        if (game.time.now > this.firingTimer)
        {
            this.enemyFires();
        }
        game.physics.arcade.overlap(this.bullets, this.enemies, this.collisionHandler, null, this);
        game.physics.arcade.overlap(this.skill1items, this.player, this.getskill1, null, this);
        game.physics.arcade.overlap(this.skill2items, this.player, this.getskill2, null, this);
        game.physics.arcade.overlap(this.skill3items, this.player, this.getskill3, null, this);
        game.physics.arcade.overlap(this.skill4items, this.player, this.getskill4, null, this);
        if(this.hurted){
            game.physics.arcade.overlap(this.enemyBullets, this.player, this.enemyBulletHitsPlayer, null, this);
            game.physics.arcade.overlap(this.enemies, this.player, this.enemyHitsPlayer, null, this);
        }
        if(this.mutebutton.isDown){
            if(game.time.now>this.muteTime){
                this.muteTime = game.time.now + 500;
                if(!this.isMute) {
                    this.isMute = true;
                    this.muteicon.bringToTop();
                    game.add.tween(this.muteicon).to( { alpha: 1 }, 500).yoyo(true).start();
                    this.bgmSound.stop();
                    game.time.events.add(1000, function(){this.muteicon.sendToBack();}, this);
                }
                else if(this.isMute){
                    this.isMute = false;
                    this.soundicon.bringToTop();
                    game.add.tween(this.soundicon).to( { alpha: 1 }, 500).yoyo(true).start();
                    this.bgmSound.play();
                    game.time.events.add(1000, function(){this.soundicon.sendToBack();}, this); 
                }
                this.muteicon.alpha = 0;
                this.soundicon.alpha = 0;
            }
        }
        this.addEnemy();
    },
    movePlayer: function() {
        if (this.cursor.left.isDown &&this.player.x>0)
        {
            this.player.x -= 10;
            this.player.animations.play('leftwalk');
        }
        else if(this.cursor.right.isDown&&this.player.x<900)
        {
            this.player.x += 10;
            this.player.animations.play('rightwalk');
        }
        else{
            this.player.animations.stop();
            this.player.frame = 5;
        }
        if(this.cursor.up.isDown&&this.player.y>0)
        this.player.y -= 10;
        else if(this.cursor.down.isDown&&this.player.y<800)
        this.player.y += 10;
        if(this.fireButton.isDown){
            if (game.time.now > this.bulletTime)
            {
                bullet = this.bullets.getFirstExists(false);
                if (bullet)
                {
                    if(!this.isMute) this.shootSound.play();
                    bullet.reset(this.player.x, this.player.y + 8);
                    if(this.skill4 == true){
                        var enem = this.enemies.getFirstAlive();
                        if(enem)
                        game.physics.arcade.moveToObject(bullet,enem,600);
                    }
                    else bullet.body.velocity.y = -600;
                    this.bulletTime = game.time.now + 200;
                }
            }   
        }
        if(this.skillbutton1.isDown && this.skill1Num>0 && game.time.now > this.skill1Cooldown){
            this.skill1Cooldown = game.time.now + 2000;
            this.skill1Num--;
            this.skill1Label.text =  'Skill1 : '+ this.skill1Num;
            if(!this.isMute) this.skill1Sound.play();
            game.global.score += 20*this.enemies.countLiving();
            this.scoreLabel.text =  'score: '+ game.global.score;
            this.createEnemy();
            this.createEnemy();
            game.camera.flash(0xffffff, 300);
            this.enemyBullets.callAll('kill');
            this.enemies.callAll('kill');

        }
        if(this.skillbutton2.isDown && this.skill2Num>0 && this.hurted){
            this.skill2Num--;
            this.skill2Label.text =  'Skill2 : '+ this.skill2Num;
            if(!this.isMute) this.skill2Sound.play();
            this.player.alpha = 0.5;
            this.hurted = false;
            game.time.events.add(5000, function(){this.player.alpha = 1; this.hurted = true;}, this);
        }
        if(this.pausebutton.isDown && this.pausecd == false){
            this.pauseicon.bringToTop();
            this.pauseicon.alpha = 1;
            game.paused = true;
        }
    },
    playerDie: function() {
        this.bgmSound.stop();
        if(!this.isMute)this.playerdieSound.play();
        this.emitter.x = this.player.x;
        this.emitter.y = this.player.y;
        this.emitter.start(true, 800, null, 15);
        game.camera.shake(0.02,300);
        game.time.events.add(1000, function(){game.state.start ('menu');}, this);
    },
    createEnemy: function() {
        var enemy1 = this.enemies.create(game.rnd.pick([100, 300, 500, 700]), 0, 'enemy1');
        enemy1.anchor.setTo(0.5, 0.5);
        enemy1.scale.setTo(0.3, 0.3);
    },

    addEnemy: function() {
        var enemy = this.enemies.getFirstDead();
        if(!enemy){return;}
        enemy.anchor.setTo(0.5, 1);
        enemy.reset(game.rnd.pick([100, 300, 500, 700]), 0);
    },
    collisionHandler: function (bullet, alien) {
        if(!this.skill3)bullet.kill();
        alien.kill();
        game.global.score += 20;
        if(game.global.score%100==0){
            this.createEnemy();
        }
        this.scoreLabel.text =  'score: '+ game.global.score;
        var random1 = game.rnd.integerInRange(0, 90);
        if(random1==2){playState.skill1emit(alien)};
        if(random1==4){playState.skill2emit(alien)};
        var random2 = game.rnd.integerInRange(0, 100);
        if(random2==2){playState.skill3emit(alien)};
        if(random2==4){playState.skill4emit(alien)};
    },
    enemyBulletHitsPlayer: function (player,bullet) {
        bullet.kill();
        live = this.lives.getFirstAlive();
        if (live)
        {
            live.kill();
        }
        if (this.lives.countLiving() < 1)
        {
            player.kill();
            this.enemyBullets.callAll('kill');
            this.playerDie();
        }
    
    },
    enemyFires: function  () {
        enemyBullet = this.enemyBullets.getFirstExists(false);
        if(enemyBullet && this.enemies.countLiving()>0)
            this.enemies.forEachAlive(function(enemy){
                playState.enemyFires2(enemy);
        });
    },
    enemyFires2: function  (enemy) {
        enemyBullet = this.enemyBullets.getFirstExists(false);
        if(!enemyBullet) {return;}
        enemyBullet.reset(enemy.body.x, enemy.body.y);
        game.physics.arcade.moveToObject(enemyBullet,this.player,350);
        this.firingTimer = game.time.now + 1000;
    },
    enemyHitPlayer: function(enemy, player){
        enemy.kill();
        live = this.lives.getFirstAlive();
        if (live)
        {
            live.kill();
        }
        if (this.lives.countLiving() < 1)
        {
            player.kill();
            this.enemyBullets.callAll('kill');
            this.playerDie();
        }
    },
    unpause: function (event){
        if(game.paused){
            game.paused = false;
            playState.pausecd = true;
            playState.pauseicon.alpha = 0;
            playState.pauseicon.sendToBack();
            playState.continueicon.bringToTop();
            game.add.tween(playState.continueicon).to( { alpha: 1 }, 500).yoyo(true).start();
            game.time.events.add(1300, function(){playState.pausecd = false;}, this);
        }
    },
    getskill1: function(SK1, player){
        player.kill();
        if(this.skill1Num<=2)this.skill1Num ++;
        this.skill1Label.text =  'Skill1 : '+ this.skill1Num;
    },
    getskill2: function(SK2, player){
        player.kill();
        this.skill2Num ++;
        this.skill2Label.text =  'Skill2 : '+ this.skill2Num;
    },
    skill1emit: function(alien){
        skill1item = this.skill1items.getFirstExists(false);
        if(!skill1item) {return;}
        skill1item.reset(alien.body.x, alien.body.y);
        skill1item.body.velocity.y = 100;
    },
    skill2emit: function(alien){
        skill2item = this.skill2items.getFirstExists(false);
        if(!skill2item) {return;}
        skill2item.reset(alien.body.x, alien.body.y);
        skill2item.body.velocity.y = 100;
    },
    getskill3: function(SK1, player){
        player.kill();
        if(this.skill4) this.skill4 = false;
        this.skill3 = true;
    },
    getskill4: function(SK2, player){
        player.kill();
        if(this.skill3) this.skill3 = false;
        this.skill4 = true;
    },
    skill3emit: function(alien){
        skill3item = this.skill3items.getFirstExists(false);
        if(!skill3item) {return;}
        skill3item.reset(alien.body.x, alien.body.y);
        skill3item.body.velocity.y = 100;
    },
    skill4emit: function(alien){
        skill4item = this.skill4items.getFirstExists(false);
        if(!skill4item) {return;}
        skill4item.reset(alien.body.x, alien.body.y);
        skill4item.body.velocity.y = 100;
    },
    iVolume: function (event){
        if(playState.vol<1){
            playState.vol += 0.1;
            playState.bgmSound.volume = playState.vol;
            playState.playerdieSound.volume = playState.vol;
            playState.shootSound.volume = playState.vol;
            playState.skill1Sound.volume = playState.vol;
            playState.skill2Sound.volume = playState.vol;
        }
    },
    dVolume: function (event){
        if(playState.vol>0){
            playState.vol -= 0.1;
            playState.bgmSound.volume = playState.vol;
            playState.playerdieSound.volume = playState.vol;
            playState.shootSound.volume = playState.vol;
            playState.skill1Sound.volume = playState.vol;
            playState.skill2Sound.volume = playState.vol;
        }
    }
}
